package clue.editor;

import java.awt.BorderLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class ComponentsPanel extends JPanel{

	private JSplitPane componentsSplitPane;
	private PlayersPanel playersPanel;
	private WeaponsCharactersPanel weaponsPanel;
	
	public ComponentsPanel()
	{
		setLayout(new BorderLayout());
		
		componentsSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		componentsSplitPane.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		componentsSplitPane.setDividerLocation(0.5);
		componentsSplitPane.setResizeWeight(0.5);
		playersPanel = new PlayersPanel();
		weaponsPanel = new WeaponsCharactersPanel();
		
		componentsSplitPane.setLeftComponent(playersPanel);
		componentsSplitPane.setRightComponent(weaponsPanel);
		
		add(componentsSplitPane, BorderLayout.CENTER);
	}

}
