package clue.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import objects.Character;

public class PlayerPanel extends JPanel {

	private JRadioButton humanRadio;
	private JRadioButton AIRadio;
	private JTextField name;
	private JComboBox<Character> charctersCombo;
	private JLabel nameTitle;
	private JButton removeButton;
	
	public PlayerPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createLineBorder(Color.BLUE));
		
		humanRadio = new JRadioButton("Human");
		AIRadio = new JRadioButton("AI");

		nameTitle = new JLabel("Name:");

		charctersCombo = new JComboBox<Character>(Character.values());
	
		removeButton = new JButton("Remove Player");
		
		name = new JTextField(10);
		
		JPanel namePanel = new JPanel();
		
		namePanel.setLayout(new BorderLayout());
		
		namePanel.add(nameTitle, BorderLayout.WEST);
		
		namePanel.add(name, BorderLayout.CENTER); 
		
		namePanel.add(charctersCombo, BorderLayout.SOUTH);
		
		add(namePanel, BorderLayout.NORTH);
		
		JPanel radioPanel = new JPanel();
		
		radioPanel.add(humanRadio);
		radioPanel.add(AIRadio);
		
		add(radioPanel, BorderLayout.CENTER);
		
		add(removeButton, BorderLayout.SOUTH);
		setVisible(true);
		
	}
	
}
