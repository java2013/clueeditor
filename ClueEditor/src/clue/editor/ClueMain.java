package clue.editor;

import javax.swing.SwingUtilities;

public class ClueMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{

		SwingUtilities.invokeLater(new Runnable() 
									{
										public void run() 
										{
											new ClueFrame();
										}
									}
								  );
	}
}
