package clue.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class CluePanel extends JPanel {

	private JSplitPane clueSplitPanel;
	private ComponentsPanel componentsPanel;
	private BoardPanel boardPanel;
	
	public CluePanel()
	{
		setLayout(new BorderLayout());

		setSize(Toolkit.getDefaultToolkit().getScreenSize());

		clueSplitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		clueSplitPanel.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		clueSplitPanel.setDividerLocation(0.65);
		

		componentsPanel = new ComponentsPanel();
		boardPanel = new BoardPanel();
		
		clueSplitPanel.setLeftComponent(boardPanel);
		clueSplitPanel.setRightComponent(componentsPanel);
		
		add(clueSplitPanel, BorderLayout.CENTER);
	}
}
