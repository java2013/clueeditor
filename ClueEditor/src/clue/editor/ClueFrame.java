package clue.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ClueFrame extends JFrame {

	
	
	public ClueFrame() {
		
		try {
			UIManager.setLookAndFeel(
				    "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		setTitle("The Clue"); // set the frame's title

		// set the frame's Close operation
				addWindowListener(new WindowAdapter() {
				});
					

				Dimension screenSize = 
					Toolkit.getDefaultToolkit().getScreenSize();

				Dimension frameSize = new Dimension();
				frameSize.setSize(screenSize.width, screenSize.height);
				setSize(frameSize);
				
				getContentPane().setLayout(new BorderLayout());
			
				setLocationRelativeTo(null);
				setVisible(true);	
				
				add(new CluePanel());
				
		}

	
}
