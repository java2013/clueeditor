package clue.editor;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.plaf.BorderUIResource;

public class PlayersPanel extends JPanel {

	private JButton addPlayerButton;
	private JPanel playersInnerPanel; 
	
	public PlayersPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Players:"));
		
		addPlayerButton = new JButton("Add Player");
		add(addPlayerButton, BorderLayout.NORTH);
		playersInnerPanel = new JPanel();
		playersInnerPanel.setLayout(new GridLayout(0,2,10,10));
		playersInnerPanel.add(new PlayerPanel());
		playersInnerPanel.add(new PlayerPanel());
		playersInnerPanel.add(new PlayerPanel());
		playersInnerPanel.add(new PlayerPanel());
		playersInnerPanel.add(new PlayerPanel());
		JScrollPane scroller = new JScrollPane(playersInnerPanel);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		add(scroller);
	}
}
