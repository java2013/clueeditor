package clue.editor;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class WeaponsCharactersPanel extends JPanel{

	private JPanel weaponsCharactersPanel; 
	
	public WeaponsCharactersPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Weapons&Characters:"));
		
		weaponsCharactersPanel = new JPanel();
		weaponsCharactersPanel.setLayout(new GridLayout(0,2,10,10));
		JScrollPane scroller = new JScrollPane(weaponsCharactersPanel);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		add(scroller);
	}
}
